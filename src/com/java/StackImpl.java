package com.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StackImpl implements StackOperations {

    private List<String> list = new ArrayList<>();

    @Override
    public List<String> get() {
        List<String> stackReprezentation = new ArrayList<>();
        int stackSize = this.list.size();
        for (int i = 0; i < stackSize; i++){
            stackReprezentation.add(list.get(stackSize - i - 1));
        }
        return stackReprezentation;
    }

    @Override
    public Optional<String> pop() {
        int stackSize = this.list.size();
        if (!list.isEmpty()){
            String lastElementList = this.list.get(stackSize - 1);
            this.list.remove(stackSize - 1);

            return Optional.of(lastElementList);
        }
        else {
            return Optional.empty();
        }
    }

    @Override
    public void push(String item) {
        list.add(item);
    }
}

package com.java;

public class Main {

    public static void main(String[] args) {
        StackOperations stack = new StackImpl();
        stack.push("1");
        stack.push("2");
        stack.push("3");
        System.out.println("Stos: " + stack.get());
        System.out.println("Pobranie el. ze stosu: " + stack.pop());
        System.out.println("Pobranie el. ze stosu: " + stack.pop());
        System.out.println("Pobranie el. ze stosu: " + stack.pop());
        System.out.println("Pobranie el. ze stosu: " + stack.pop());
    }
}
